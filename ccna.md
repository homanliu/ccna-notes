## OSI Reference Model

### OSI Model

| Layer 7     | Application Layer      |
| ----------- | ---------------------- |
| **Layer 6** | **Presentation Layer** |
| **Layer 5** | **Session Layer**      |
| **Layer 4** | **Transport Layer**    |
| **Layer 3** | **Network Layer**      |
| **Layer 2** | **Data Link Layer**    |
| **Layer 1** | **Physical Layer**     |

#### Level 7 - Application Layer

* Represents protocols that directly support user applications
* e.g. **HTTP**, **FTP**, **TFTP**, **Telent**, **SMTP**

#### Layer 4 - Transport Layer

* Segments and reassembles data to be transmitted efficiently
* Data from different applications can be sent to remote host at the same time

#### Layer 3 - Network Layer

* Responsible for delivery of data from source to destination host
* Data can be directed to designated network by layer-3 devices e.g. **Router**
* Router maintains routing tables which provide information on how to forward data

#### Layer 2 - Data Link Layer

* Hardware addresses are also called MAC address (Media Access Control)
* Bridges and switches are layer-2 devices that can reduce traffic congestion
* Error detection happened on data frames
* Frame check sequence (FCS) field inside Data Link Layer header
* Calculated using Cyclic Redundancy Check (CRC) Algorithm

## Ethernet

#### Logical Bus Design

* Provide shared connection only
* Half-Duplex Ethernet
* Example: Hub (Type: shr)

#### Logical Switching Design

* Provide dedicated connection
* Full-Duplex Ethernet
* Example: Switch (Type: p2p)

#### Unshielded Twisted Pair (UTP) Cable

##### Straight-through UTP cable

* (PIN 1) Orange White, Orange, Green White, Blue, Blue White, Green, Brown White, Brown
* Same ordering for both sides

##### Crossover UTP cable

* (PIN 1) Orange White, Orange, Green White, Blue, Blue White, Green, Brown White, Brown
* (PIN 1) Green White, Green, Orange White, Blue, Blue White, Orange, Brown White, Brown
* Orange White &hArr; Green White, Orange &hArr; Green

#### Media Dependent Interface (MDI) and Media Dependent Interface Crossover (MDI-X)

##### Media Dependent Interface (MDI)

* Wire 1, 2 for data transmission and wire 3, 6 for data reception
* Example: Router, Network card

##### Media Dependent Interface Crossover (MDI-X)

- Wire 1, 2 for data reception and wire 3, 6 for data transmission
- Example: Switch, Hub

##### Connection

* Straight-through UTP cable for **MDI** to **MDI-X**
* Crossover UTP cable for **MDI** to **MDI** or **MDI-X** to **MDI-X**

#### Rollover Console Cable

* Connect console port of Cisco devices to serial port of computer to configure devices

#### Half-duplex Ethernet

* Collision may happen in logical bus design
* Impossible to transmit and receive data at the same time
* CSMA/CD regulates network traffic and reduce chance of collision
* CSMA/CD stands for **Carrier Sense Multiple Access with Collision Detection**

##### Carrier Sense

* Each computer uses the reception pair of wires to listen until the media is silent before transmission

##### Collision Detection

* Computer continues to receive its own transmitting data
* Perform backoff process by stopping transmission if it detects corrupted data
* Wait for random time and re-transmit again

#### Full-duplex Ethernet

* Possible to transmit and receive data at the same time
* Doubled bandwith comparing with Half-duplex ethernet

#### Auto Negotiation

* Determine which duplex mode should be implemented

#### Duplex Mismatch

* Situation: operating half-duplex and full-duplex within same environment
* Results itermittently slow performance
* For half-duplex port, counter value of late collision will increase if duplex mismatch

```bash
sh int f0/0
```

## Detail Concepts on Logical Switching

#### Ethernet Addressing

* MAC address consists of 48 bits
* First 24 bits are vendor's code / Organizationally Unique Identifier (OUI) assigned by IEEE

#### Switching Mechanism

* If destination address of data is not found on address table, data is forwarded to all ports except sender port
* Broadcast data frame with destination MAC address **FF-FF-FF-FF-FF-FF**

| Harware Address   | Port |
| ----------------- | ---- |
| 00-00-00-11-11-11 | 1    |
| 00-00-00-22-22-22 | 2    |

#### Aging Time

* Default aging time: 300 seconds
* Remove entry in address table if no longer receive data from source MAC address

#### Presence of Hubs

* Same port will be recorded if connecting to hubs and a switch port

| Hardware Address  | Port |
| ----------------- | ---- |
| 00-00-00-33-33-33 | 3    |
| 00-00-00-44-44-44 | 3    |

#### Broadcast Domain

* Cannot provide separate broadcast domains in LAN for hubs and switches
* Router does not forward broadcast data frames

## Features Provided By Cisco Switches

#### Spanning Tree Protocol (IEEE 802.1d)

* Provide redundant path for fault tolerance
* Form infinite loop and cause broadcast storms

#### Spanning Tree Protocol (STP) Mechanism

* Communication of STP messages, **Bridge Protocol Data Unit** (BPDU)


* Avoid switching loop by placing port into forwarding state or blocking state
* Elect a switch to be **Root Bridge**
  * Highest priority (lowest priority value): 32768 (Default)
  * Lowest value in MAC address
  * All ports of Root Bridge will be forwarding state
* Port has lowest cumulative path cost to Root Bridge is called Root Port with forwarding state

| Bandwidth | Cost |
| --------- | ---- |
| 10 Gbps   | 2    |
| 1 Gbps    | 4    |
| 100 Mbps  | 19   |
| 10 Mbps   | 100  |

*  Remaining ports will be blocking state
* Designated Port / Root Port = Forwarding state
* Althernate Port = Blocking state

#### Different States in Spanning Tree Protocol (STP)

**Blocking State**

* Cannot receive or send data frames
* Can receive but cannot send STP messages

**Listening State**

* Cannot receive or send data frames
* Can receive and send STP messages
* Last for 15 seconds by default

**Learning State**

* Can learn MAC address, but cannot receive or send data frames
* Can receive and send STP messages
* Last for 15 seconds by default

**Forwarding State**

* Can learn MAC address and can receive or send data frames
* Can receive and send STP messages

#### Bridge ID Priority

* Priority + VLAN identification number
* Configurable priority values: 0, 4096, 8192, 12288, 16384, 20480, 24576, 28672, 32768, …, 61440
* Increment of 4096 in range of 0 to 61440

#### PortFast

* Make ports to go immediately from Blocking State to Forwarding State without 30 seconds delay
* Type: Edge p2p
* Receiving a BPDU on PortFast enabled port signals abnormal environment
* Enable BPDU guard
* When receives BPDU, port will become error-disabled state which cannot send and receive traffic

#### Rapid Spanning Tree Protocol (RSTP)

* Defines how switches interact with each other to keep loop free with faster convergence

**Discarding State**

* Combine STP blocking and listening state

**Learing State**

**Forwarding State**

## Virtual Local Area Network (VLAN)

* VLAN is a group of switch ports
* Configuring multiple VLANs **logically divided switch** into multiple independent switches at layer 2
* **Increase** number of broadcast domains and **decrease** size of broadcast domain

#### Communication of VLAN between Switches

* VLAN can span multiple switches
* Connected with crossover UTP cable

#### Inter-Switch Link (ISL)

* **Trunk link** by default allows all VLANs traffic to flow between switches
* Data is exchanged using **ISL Protocol** with ISL frames
* Ethernet frame is encapsulated with **ISL header information** which included VLAN ID

| ISL Header |                 |        |
| ---------- | --------------- | ------ |
| VLAN 3     | Ethernet Header | Packet |

#### 802.1Q (Providing Trunk Link)

* No VLAN tag for native VLAN data frames
* All untagged traffic received on 802.1Q trunk port is forwarded to native VLAN configured
* Native VLAN of both switches must match each other (**native VLAN mismatch**)

#### Dynamic Trunking Protocol (DTP)

| DTP switchport mode | Status                            | Competible trunking mode to form trunk link |
| ------------------- | --------------------------------- | ------------------------------------------- |
| trunk               | Permanent to perform trunking     | trunk, dynamic desirable, dynamic auto      |
| dynamic desirable   | Request to perform trunking       | trunk, dynamic desirable, dynamic auto      |
| dynamic auto        | Willing to perform trunking       | trunk, dynamic desirable                    |
| access              | Permanent not to perform trunking | N/A                                         |

#### VLAN Trunking Protocol (VTP)

* VTP can propagate VLAN information through trunk links to all switches with same VTP domain name
* Update VLAN information **in all switches** in same VTP domain
* VTP update or VTP advertisement

#### VTP Modes

##### VTP Server Mode

* VLAN information can be created, modified or deleted locally in switch
* Update by other switches and pass information to other switches

##### VTP Client Mode

* Update by other switches and pass information to other switches

##### VTP Transparent Mode

* VLAN information can be created, modified or deleted locally in switch
* Local VLAN information cannot be used to update other switches
* Pass VLAN information received to other switches

#### VTP Advertisement

* Start with **Configuration Revision** number 0
* Use higher revision number VTP information
* Configuration Revision number is reset to 0 when VTP transparent mode

#### Inter-VLAN Routing

* Layer 3 configured with IP routing or router is needed to route data through VLANs
* Trunk link connection between router and switch is called **Router-on-a-stick**
* Router physical interface must be configured with multiple IP addresses through sub-interfaces

#### EtherChannel

* Provide more bandwidth for connection between switches
* Bind two or more ethernet links together to form single logical channel
* STP treats EtherChannel as single link

| EtherChannel Mode      | Description                                                  |
| ---------------------- | ------------------------------------------------------------ |
| on                     | Turn on with **no negotiation**, another side must be also on |
| desirable (Cisco)      | Send PAgP (Port Aggregation Protocol) packets to request formation of EtherCahnnel |
| auto (Cisco)           | Send PAgP (Port Aggregation Protocol) packets if received    |
| active (IEEE 802.3ad)  | Send LACP (Link Aggregation Control Protocol) packets to request formation of EtherCahnnel |
| passive (IEEE 802.3ad) | Send LACP (Link Aggregation Control Protocol)packets if received |

* All interfaces in EtherChannel should have same speed

## IP Address

#### Network Representation

* Host ID with all 0s in binary representation
* Not a host

#### Directed Boardcast

* Host ID with all 1s in binary representation
* When directed boardcast packets arrived to router that is **directly connected** to destination network, router may encapsulate packet in frame with **destination MAC address** FF-FF-FF-FF-FF-FF

#### Different Network Classes

##### Class A

* First bit must be 0
* 1.0.0.0 - 126.255.255.255
* 0.0.0.0 - 0.255.255.255 reserved as default address
* 127.0.0.0 - 127.255.255.255 reserved as loopback testing
* Subnet Mask: 255.0.0.0 (/8)

##### Class B

* First 2 bits must be 10
* 128.0.0.0 - 191.255.255.255
* Subnet Mask: 255.255.0.0 (/16)

##### Class C

* First 3 bits must be 110
* 192.0.0.0 - 223.255.255.255
* Subnet Mask: 255.255.255.0 (/24)

##### Class D

* 244.0.0.0 - 239.255.255.255
* Multicast addresses
* Content of multicast packet is read by a specific group of hosts

##### Class E

* 240.0.0.0 - 255.255.255.254
* Local broadcast: 255.255.255.255

## TCP/IP

#### Connection-Oriented Protocol

* Example: TCP

#### 3-way Handshake

1. SYN packet: send with its own starting sequence number
2. SYN ACK packet: send with its own starting sequence number and acknowledge receiving SYN packet
3. ACK packet: acknowledge receiving SYN packet

#### Windowing

* Maximum amount of unacknowledged data segments that sender can send before it stops and waits for an acknowledgement

#### Connectionless Protocol

* Example: UDP
* Provided fewer features, smaller overhead

#### Internet Protocol

* Provide routing capabilities to carry data to another network

#### Address Resolution Protocol (ARP)

* Request interface with specified IP address to return its MAC address
* With destination MAC address FF-FF-FF-FF-FF-FF

1. Host A sends ARP request asking all interfaces in network with IP address 10.0.0.1 to return MAC address
2. Return to Host A with its own MAC address through ARP reply
3. ARP cache is updated in host A

#### Internet Control Message Protocol (ICMP)

* ICMP **Destination Unreachable** sent from router to inform host that data cannot be sent
* ICMP **Echo Request** and **Echo Reply** messages often use to test connections

##### Time Exceeded

* **Time To Live** (TTL) values are designed to prevent data packets from looping endlessly
* Router subtracts 1 from TTL value
* Drop the packet and return ICMP **Time Exceeded** to source if TTL = 0

## Overview of Cisco Routers

#### Read Only Memory (ROM)

* Contain power-on diagnostic code to perform self-test
* System bootstrap code to search for full Cisco IOS image

#### Flash Memory

* Store fully functional IOS image

#### Non-volitile RAM (NVRAM)

* Store start-up configuration file

##### Primary RAM

* Store running configuration, routing table and ARP mappings

##### Shared RAM

* Store incoming and outgoing packets

#### Data Communication Equipment (DCE) and Data Terminal Equipment (DTE)

* Synchronous serial communication, DCE &hArr; DTE
* DCE provided clock signal to pace communication with DTE
* WAN Protocol: its header does not have MAC address

## Dynamic Routing

* Most specific mask among all matched entries will be used for routing
* Connected interface with administrative distance: 0
* Static route with administrative distance: 0

#### Distance Vector Routing Protocol

* Example: RIP
* Regularly advertise most entries of routing table to neighboring routers
* Re-advertise only best path for network
* Hop count is **number of routers** before reaching destination

#### Routing Information Protocol (RIP)

* Administrative Distance: 120
* Route with **least administrative distance** will be used
* Metric value: hop count
* If two or more routes have same hop count, router will uses all those routes to **load share the data traffic**
* RIPv2 sends RIP routes through **multicast packets with destination IP 224.0.0.9**
* Maximum metric paths for load sharing: 4 (Default)
* Send routing table information to its neighbours regularly

#### Enhanced Interior Gateway Routing Protocol (EIGRP)

* Feature of both distance vector routing protocol and link state routing protocol
* Balanced hybrid routing protocol
* Administrative Distance: 90
* Metric Value: based on bandwidth and delay
* EIGRP sends EIGRP routes through **multicast packets with destination IP 224.0.0.10**

##### EIGRP Neighbor Table

* Routing table updates are not sent regularly
* Send **Hello Packet** regularly

##### EIGRP Tpology Table

* Exchange EIGRP update packet that contains routing information of reachable networks
* Create and maintain by **Update Packet**
* Lowest metric routing entry will be copied into routing table

#### Auto Summary

* When router is crossing major network boundary, RIP and EIGRP will perform auto-summary
* Only one EIGRP update packet instead of advertising individual subnet
* Reduce badwidth usage and router processor, memory
* Problem for environment with discontiguous subnets

#### Open Shortest Path First (OSPF)

* Link State dynamic routing protocol
* Administrative Distance: 110
* OSPF sends OSPF routes through **multicast packets with destination IP 224.0.0.5 or 224.0.0.6**
* OSPF send updates (e.g. link up or down) to all other routers in the same area
* OSPF will elect Designated Router (DR) and Backup Designated Rotuer (BDR) **from broadcast multiaccess** which are not point-to-point
* OSPF priority value &rarr; IP address among logical interfaces &rarr; IP address among physical interfaces
* Updates will only send to DR and BDR via 224.0.0.6, then DR will send updates to all other routers via 224.0.0.5
* If router is in-between two area, then it called **Area Border Router (ABR)**
* ABE has ability to allow any numbers of router updates to pass from area to area
* Total path cost is calculated by $$\sum {100 \over {Bandwidth\ (Mbps)}}$$

##### OSPF Neighbor Table

* Hello Packet is also used and sent with destination IP 224.0.0.5
* Same Hello Interval and Dead Interval will be used to form adjacency relationship

##### OSPF Topological Database

* Each OSPF router will exchange Link State Advertisement (LSA) packets and store to its OSPF topological database
* Each OSPF router calculates shortest path to each network based on topological database

##### Process involved OSPF

1. Exchange Hello Packets
2. Build up and maintain neighbor table
3. Exchange Link State Advertisement
4. Build up and maintain topological database
5. Create routing entries in the routing table

##### Limitation for OSPF multiple area

* All areas must be connected to Area 0
* Area 0 is also called backbone area

## Access Lists

* If no matching is found in access list, **the data will be dropped**

#### Access List Types

| Access List Number | Types                    |
| ------------------ | ------------------------ |
| 1 - 99             | IP Standard Access List  |
| 100 - 199          | IP Extended Access List  |
| 800 - 899          | IPX Standard Access List |
| 900 - 999          | IPX Extended Access List |

| IP Standard Access List | IP Extended Access List             |
| ----------------------- | ----------------------------------- |
| Source IP address       | TCP/IP protocol type                |
| Source IP wildcast mask | Source/Destination IP address       |
|                         | Source/Destination IP wildcast mask |
|                         | Source/Destination port number      |

* Only 1 access list for same Layer 3 Protocol, same interface, same direction
* Access List can be used to filter inbound or outbound data
* Extended access lists should be placed as close as possible to source of traffic to be denied

## Wide Area Networking (WAN) Protocols

* Leased line is a point-to-point WAN type with highly flexible bandwidth scaling

#### High-level Data Link Control (HDLC)

* Not provide authentication and error correction
* Support multiple upper layer protocol (IP, IPX) since it has field in header to identify the network layer protocol

#### Point-to-Point Protocol (PPP)

* Provide authentication and error correction
* Support multiple upper layer protocol (IP, IPX) since it has field in header to identify the network layer protocol
* An interface line protocol is declared down after three update intervals without receiving keepalive packet

#### PPP Authentication Protocols

##### Password Authentication Protocol (PAP)

##### Challenge Handshake Authentication Protocol (CHAP)

* CHAP transmits encrypted password
* Verify identity of opposite device by using three-way handshake
* Based on a shared secret password
  1. Router1 sends a challenge message to Router2
  2. Router2 responds with calculated value combined with shared secret password
  3. Router1 checks the value. If match, acknowledges success authentication
* Send a new challenge at random intervals

#### Point-to-Point Protocol over Ethernet (PPPoE)

* Protocol for encapsulating PPP frames inside Ethernet frames
* PPPoE connection involves **Active Discovery Phase** and **PPP Session Phase**
* Active Discovery Phase can ensure MAC address of PPPoE client and server known each other, then establish session ID for further exchange of packets
* PPP Session Phase can ensure options are negotiated and authentication is performed

## Cisco Discovery Protocol (CDP)

* CDP is a layer 2 protocol used by Cisco devices to obtain information about neighboring devices
* Link Layer Discovery Protocol (LLDP) is used for non-cisco devices

## NAT, DHCP and NTP

#### Network Address Translation (NAT)

* NAT conserves the use of legally registered IP addresses

##### Dynamic NAT with Overload

* It offered an effective use of public IP addresses as a singfle public IP address can be shared by multiple devices
* Port Address Translation (PAT) is used
* Static NAT mapped a port number of public address to NAT client permanently

#### Dynamic Host Configuration Protocol (DHCP)

* If renewal period passed, DHCP client try to keep same IP address by renewing its lease through periodic unicast contact of DHCP server
* If rebind peroid expired, DHCP client will broadcast rebinding request to any available DHCP server periodically

#### Network Time Protocol (NTP)

* NTP is used to synchronise clocks of various devices with UDP 123

#### DHCP Conflict

* The conflicted IP address will be removed from the pool until resolved

##### Detection Through Ping

* DHCP server pings IP address to be assigned by using ICMP
* If response is received, IP address conflict will be recorded in DHCP conflct table

##### Detection Through Gratuitous ARP

* When DHCP client received an IP address from server, it will send a gratuitous ARP for checking conflict of IP address
* If a host with duplicated IP address, the host will send a **response to DHCP client**
* DHCP client declines the use of assigned IP address and sends **DHCPDECLINE message to DHCP server**


## IP Version 6

* IPv6 increases number of bits from 32bit to 128bit
* ::1 is IPv6 loopback address = 127.0.0.1 in IPv4

#### Global Unicast Address

| Global Routing Prefix | Subnet  | Interface ID |
| --------------------- | ------- | ------------ |
| 48 bits               | 16 bits | 64 bits      |

* Global Routing Prefix consists of 48 bits and first 3 bits are 001
* Starting field of hexadecimal digits: 2000 - 3FFF
* Global Routing Prefixed are allocated by IANA

#### Unique Local Address

* Simliar idea as Local IP Address in IPv4 which are used in private network
* Starting with bits 1111 110 **OR** FC00::/7

| FD   | Global ID             | Subnet  | Interface ID |
| ---- | --------------------- | ------- | ------------ |
|      | Pseudo-Random 40 bits | 16 bits | 64 bits      |

* Defined for FD00::/8
* Global ID can uniquely identify the network

#### More About IPv6 Addressing

* IPv6 does not have broadcast address in order to prevent affecting irrelevant devices
* Rely on multicast addresses to send to a group of devices

| IPv6 Multicast Address | To Group of Devices                     | IPv4 Multicast Address |
| ---------------------- | --------------------------------------- | ---------------------- |
| FF02:2                 | All IPv6 routers on local segment       | 224.0.0.2              |
| FF02:5                 | All IPv6 OSPF routers on local segment  | 224.0.0.5              |
| FF02:6                 | All IPv6 OSPF DR/BDR on local segment   | 224.0.0.6              |
| FF02:9                 | All IPv6 RIP routers on local segment   | 224.0.0.9              |
| FF02:A                 | All IPv6 EIGRP routers on local segment | 224.0.0.10             |

* An interface may be assigned multiple IPv6 addresses of any type
* IPv6 devices on the same link can use link-local addresses to communicate
* FE80::/10 or starting with bits 1111 1110 10 has been reserved for link-local unicast addressing
* Link-local addresses are used to form routing adjacencies in routing table

#### Extended Unique Identifier - 64 (EUI-64)

* Interface ID of link local address is created according to EUI-64
* MAC address consists of 48 bits
* 0xFFFE is inserted in the middle between first and last 24 bits
* 7th bit will be complemented

#### IPv6 Auto-configuration

* IPv6 allows a host to automatically generate a global unicast address without manual configuration or DHCP

#### IPv6 Anycast

* An IPv6 anycast address is an IPv6 unicast address assigned to set of interfaces that belong to different devices
* Packet is sent to IPv6 anycast address is delivered to the nearest interface
* IPv6 anycast address is known as IPv6 one-to-nearest addressing

#### IPv4 to IPv6 Transition Strategies

1. Dual Stack
2. Network Address Translation (NAT)
3. Overlay Tunneling

##### Dual Stack

* Both IPv4 and IPv6 will be run in the networks 

##### Network Address Translation Protocol Translation (NAT-PT)

* Similar idea from NAT, provide mapping with IPv4 and IPv6 addresses

##### Overlay Tunneling

* IPv6 packets are encapsulated with an IPv4 header by the border dual-stack router and send through IPv4 networks to other border dual-stack router at other end
* Both tunnel endpoints must support both IPv4 and IPv6 protocol stacks

| IPv4 Header | GRE Header | IPv6 Header | IPv6 Data |
| ----------- | ---------- | ----------- | --------- |
| 20 bytes    | 4 bytes    |             |           |

## System Message Logging (SYSLOG)

* System messages can be sent to different destination, e.g. console, syslog servers

| Level         | Numeric Code | System Condition                 |
| ------------- | ------------ | -------------------------------- |
| Emergency     | 0            | System unusable message          |
| Alert         | 1            | Take immediate action            |
| Critical      | 2            | Critical condition               |
| Error         | 3            | Error message                    |
| Warning       | 4            | Warning message                  |
| Notification  | 5            | Normal but significant condition |
| Informational | 6            | Information message              |
| Debug         | 7            | Debug message                    |

## Simple Network Management Protocol (SNMP)

* SNMP is an application-layer protocol for monitoring and management between managers and agents
* SNMP manager can send SNMP agent requests with destination UDP 161 to get and set values
* Traps are messages alerting SNMP manager to a condition on network
* Informs are traps that includes a request for confirmation (ACK)

#### SNMP Version 1

* Security is based on common strings
* An SNMP manager can get and set a value
* An SNMP agent can send SNMP traps to SNMP manager

#### SNMP Version 2 (SNMPv2c)

* Addition to SNMPv1
* An SNMP manager can getbulk a large amount of values
* Sending SNMP informs from agent to manager

#### SNMP Version 3

Authentication:		Determine the message is from valid source through username

Message integrity:	Ensure packet has not changed through MD5 or SHA

Encryption:			Encrypting the contents of packet through DES

## Quality of Service (QoS)

* Voice over IP traffics has few characteristics that different from normal
  * Low Latency
  * Small Packet Size
  * Traffic rate is constant even if slient
  * Retransmission is not neccessary
* QoS refers to traffic prioritization and resource reservation control

#### IntServ Model

* To guarantee from source to destination, resources have to be reserved along the path before sending
* **Resource Reservation Protocol (RSVP)** is signaling protocol used for IntServ Model
* Signal all routers along the path and request to reserve neccessary bandwidth
* RSVP Path Message: QoS Information and Path Information
* PSVP Reservation Message: QoS Information and Results
* If reservation fails in any routers, the originating application will not send traffic

#### DiffServ Model

* All routers along the path act individually to prioritize traffic without signaling to coordinate in between
* Marking of **Type of Service (ToS)** field in IP header with 8 bits
* There are two forms of marking ToS field

##### IP Precedence

* Use only most 3 most significant bits

| IP Precedence Bits | Decimal Value | Description      |
| ------------------ | ------------- | ---------------- |
| 000                | 0             | Routine          |
| 001                | 1             | Priority         |
| 010                | 2             | Immediate        |
| 011                | 3             | Flash            |
| 100                | 4             | Flash Override   |
| 101                | 5             | Critical         |
| 110                | 6             | Internet Control |
| 111                | 7             | Network Control  |

##### Differentiated Services Code Point (DSCP)

* Use only 6 most significant bits

| DSCP Bits | Name    | Description                |
| --------- | ------- | -------------------------- |
| 000 000   | Default | Best-effort service        |
| 001 *dd*0 | AF1     | Assured Forwarding Class 1 |
| 010 *dd*0 | AF2     | Assured Forwarding Class 2 |
| 011 *dd*0 | AF3     | Assured Forwarding Class 3 |
| 100 *dd*0 | AF4     | Assured Forwarding Class 4 |
| 101 110   | EF      | Expedited Forwarding       |

* The bits *d* are used for drop probability

| *dd* Value | Drop Probability |
| ---------- | ---------------- |
| 01         | Low              |
| 10         | Medium           |
| 11         | High             |

#### Class Based Weighted Fair Queue (CBWFQ)

* Packets are assigned to a class into a queue
* Each class is configured with a bandwidth value to indicate minimum bandwidth when there is congestion

#### Low Latency Queuing (LLQ)

* For time-sensitive traffic with constant data rate
* If there are packets arrived in that priority queue, those packets do not have to wait and send out immediately
* Traffic being put into priority queue is restricted and cannot exceed its guaranteed bandwidth when there is congestion

#### Congestion Avoidance

* TCP global synchronization happened if all TCP sessions reduced and increased rate at the same time
* **Random Early Detect (RED)** randomly drop packets before a queue is full

#### Policing

* Traffic policing can identify whether traffic rate is under or over a specified value
* Prevent a class from using an excessive amount of available bandwidth

#### Shaping

* If traffic rate is over, packets of excessive traffic will be buffered and sent later
* Smooth out the temporary traffic bursting over the specified rate to produce a steadier flow

## IP Service Level Agreement (SLA)

* Provide active monitoring and simulate traffic of different applications or services

## AAA with TACACS+ and RADIUS

* AAA stands for Authentication, Authorization and Accounting
* Centralize username/password and other information in a security server
* RADIUS stands for Remote Authentication Dial In User Service
* RADIUS encrypts only password in access-request packet
* TACACS stands for Terminal Access Control Access Controller Server
* TACACS encrypts entire body of access-request packet

Authorization:		Permit and/or restrict user to access resources

Accounting:			Keep track of resources that user used

* Wireless LAN controller can be configured to work with multiple lightweight APs
* User does not need to perform authentication again

## Multi-Protocol Label Switching (MPLS)

* MPLS requires use of Cisco Express Forwarding (CEF)
* There are three kinds of router switching mechanisms

#### Process Switching

* Perform recursive lookup in its routing table to determine how packet has to be forwarded

#### Fast Switching

* Outgoing interface and information for constructing layer 2 header will be cached
* Full lookup does not need to be performed for subsequent packet

#### Cisco Express Forwarding (CEF)

* CEF increases the performance by building a **Forwarding Information Base (FIB)** table
* Avoid inefficiency recursive lookup in the routing table
* FIB table is created automatically from routing table
* To avoid duplicate information to be stored in FIB table, layer 2 header information is stored in another table known as adjacency table

#### MPLS

* A short 32-bit label is added behind layer 2 header
* Each MPLS router along the path uses this label to forward packet instead of using destination IP

| Label                                       | Experimental Field                    | Bottom-of-stack-indicator                    | TTL Field |
| ------------------------------------------- | ------------------------------------- | -------------------------------------------- | --------- |
| 20 bits to determine how to forward packets | 3 bits used as Class of Service (CoS) | 1 bit to indicate whether this is last label | 8 bits    |

* Control Plane is responsible for exchange of routing information
* Data Plane is responsible for forwarding of packets

## Hot Standby Routing Protocol (HSRP)

* HSRP can be used to provide a redundant default gateway for the hosts
* HSRP provides redundancy for IP networks ensuring that data traffic transparently recovers from first-hop router failures
* HSRP is a kind of **First Hop Redundancy Protocol (FHRP)**

## Border Gateway Protocol (BGP)

#### Autonomous System (AS)

* Unique AS number is assigned for indentification in order to propagate routing information between different organisations and ISPs

#### Usage of BGP

* Routing between different ASs, e.g. between ISPs in the Internet
* ISP can learn through BGP which AS should a data packet be routed

## Miscellaneous

#### More About DNS

* DNS server can serve two different functions:
  1. Hold host's information for a domain so that it can answer other queries, known as **Authoritative DNS**
  2. Provide DNS response to DNS client by performing recursive lookup, known as **Recursive DNS Server**
  3. Cache the response in order to answer another DNS client, known as **Caching DNS Server**

#### Split Horizon - Routing Loop Avoidance Mechanism

* Reduce the chance of routing loop formation
* Prevent a router from sending router entries back in the direction from which routing entries came

#### Cut-through Switching Mode

* Read the destination address when first 6 bytes of data frame arrives
* Forward immediately and does not wait for whole data frame before forwarding
* No error checking is performed for this switching mode

#### Store-and-Forward Switching Mode

* Receive and store whole data frame and then check FCS of data frame
* Error checking is performed for this switching mode

#### Collision Domain

* If hosts in a LAN are **connected by hubs**, they belong to the **same collision domain**

#### Switch Stacking

* Method for connecting upto 9 switches together to behave as a single switching unit for management
* All switches must be interconnected through their stack ports with stack cables
* All switches must run same release of Cisco IOS software
* The stack is managed through a master switch which is elected from one of stack members

##### Master Switch

* Responsible for maintaining running configuration
* Keep configuration current by periodically sending copies or updates to all other members