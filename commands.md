## Cisco IOS Commands

### Basic Commands

#### Different modes in Cisco IOS

```bash
# User Execution Mode
Switch>
Switch> en
# Previleged Execution Mode
Switch#
Switch# config t
# Global Configuration Mode
Switch(config)#
Switch# int f0/0
# Interface Configuration Mode
Switch(config-if)#
```

#### Display Software and Hardware Information

```bash
Switch> show version
Switch> sh ver
```

#### Display Interface Information

```
Switch# sh int f0/0
```

#### MAC Address Table Entries

```bash
Switch# sh mac address-table int f0/0
Switch# sh mac-address-table int f0/0
```

#### Static Entry in MAC Address Table

```bash
Switch(config)# mac address-table static 1122.ccdd.8899 vlan 1 int f0/1

# Delete static entry
Switch(config)# no mac address-table static 1122.ccdd.8899 vlan 1
```

#### Port Security

```bash
Switch(config-if)# switchport mode access
Switch(config-if)# switchport port-security maximum ${CONNECTION}
# Dynamically learn if mac address number < ${CONNECTION}
Switch(config-if)# switchport port-security mac-address 1122.abcd.7890
Switch(config-if)# switchport port-security violation shutdown
Switch(config-if)# switchport port-security

# Dynamically learn and save to running configuration
Switch(config-if)# switchport port-security mac-address sticky
```

#### Display Port Security

```bash
Switch# sh port-secutiry int f0/0
```

#### Enable Interface

```bash
Switch(config-if)# no shutdown
```

#### Change MAC Address for Interface

```bash
Switch(config-if)# mac-address 0011.aabb.5566

# remove setting
Switch(config-if)# no mac-address
```

#### Change Display Name of Device

```bash
Switch# hostname Switch1
Switch1#
```

#### Configure IP Address

```bash
Router(config-if)# ip address 10.0.0.1 255.0.0.0
```

### Spanning Tree Protocol (STP)

#### Display Spanning Tree Protocol Information

```bash
Switch# show spanning-tree
```

#### Change Priority of Switch in STP

```bash
Switch(config)# spanning-tree vlan 1 priority 0
```

#### STP PortFast

```bash
Switch(config-if)# spanning-tree portfast
```

#### BPDU Guard

```bash
Switch(config-if)# spanning-tree bpduguard enable
```

#### Enable RSTP

```bash
Switch(config)# spanning-tree mode rapid-pvst
```

### Switch Management

#### Configure Password for Privileged Mode

```bash
Switch(config)# enable secret ${PASSWORD}
```

#### Configure Password for Console Connection

```bash
Switch(config)# line console 0
Switch(config-line)# password ${PASSWORD}
Switch(config-line)# login
```

#### Configure Password for Telnet Connection

```bash
Switch(config)# line console 0
Switch(config-line)# line vty 0 ${MAX_USER}
Switch(config-line)# password ${PASSWORD}
Switch(config-line)# login
```

#### Display Running Configuration

```bash
Switch# show runnning-config
Switch# sh run
```

#### Password Encryption

```bash
Switch(config)# service password-encryption
```

#### Configure Default Gateway

```bash
Switch(config)# ip default-gateway 10.0.0.1
```

#### Restrict VTY Connection by Access List

```bash
Switch(config)# access-list 1 permit 10.0.0.1 0.0.0.0
Switch(config)# line vty 0 ${MAX_USER}
Switch(config-line)# access-class 1 in
```

#### Display Start-Up Configuration

```bash
Switch# show startup-config
Switch# sh start
```

#### Save Running Configuration to Start-Up Configuration

```bash
Switch# copy run start
```

#### Erase Start-Up Configuration

```bash
Switch# erase start
```

### Virtual Local Area Network (VLAN)

#### Creation of new VLAN

```bash
Switch(config)# vlan 2
Switch(config-vlan)# name ${VLAN_NAME}
```

#### Attach Port to VLAN

```bash
# By default, all ports are in VLAN 1
Switch(config)# int f0/1
Switch(config-if)# switchport mode access
Switch(config-if)# switchport access vlan 2
```

#### Native VLAN Configuration for 802.1Q

```bash
Switch(config-if)# switchport trunk native vlan 2
```

#### VLAN Trunk Configuration

```bash
# 802.1Q
Switch(config-if)# switchport trunk encapsulation dot1q
# ISL
Switch(config-if)# switchport trunk encapsulation isl
# Negotiate with neighbouring interface
Switch(config-if)# switchport trunk encapsulation negotiate

# DTP Switchport Mode
Switch(config-if)# switchport mode access
Switch(config-if)# switchport mode dynamic auto
Switch(config-if)# switchport mode dynamic desirable
Switch(config-if)# switchport mode trunk
```

#### Display Trunk Information

```bash
Switch# sh int trunk
```

## VLAN Trunking Protocol (VTP)

#### Delete VLAN Information

```bash
Switch# delete vlan.dat
```

#### Display VTP Information

```bash
Switch# sh vtp status
```

#### Configure VTP Domain Name

```bash
Switch(config)# vtp domain ${VTP_DOMAIN_NAME}
```

#### VTP Modes

```bash
Switch(config)# vtp mode server
Switch(config)# vtp mode client
Switch(config)# vtp mode transparent
```

#### VTP Password

```bash
Switch(config)# vtp password ${VTP_PASSWORD}
```

#### Display VTP Password

```bash
Switch# show vtp password
```

#### Inter-VLAN Routing

```bash
Router(config)# int f0/0
Router(config-if)# no ip address
Router(config-if)# int f0/0.1
Router(config-subif)# encapsulation ${VLAN_PROTOCOL} ${VLAN_ID} native
Router(config-subif)# ip address 10.0.0.1 255.0.0.0
Router(config-subif)# int f0/0.2
Router(config-subif)# encapsulation ${VLAN_PROTOCOL}
Router(config-subif)# int f0/0.3
Router(config-subif)# encapsulation ${VLAN_PROTOCOL}
```

#### Create EtherChannel

```bash
Switch(config)# int range f0/1 - 2
Switch(config-fi-range)# channel-group ${GROUP_NUMBER} mode {on, desirable, auto, active, passive}
```

#### Display EtherChannel Information

```bash
Switch# sh int etherchannel

Switch# sh etherchannel summary
```

#### Display ARP Cache

```bash
Switch# sh arp
```

## Basic Router Management

#### Show Flash Memory Directories

```bash
Router# sh flash
```

#### Backup Flash Memory IOS

```bash
Router# copy flash tftp
```

#### Display Serial Interface

```bash
Router# sh controllers s0/0/0
```

## Static Routing

#### Display Routing Table

```bash
Router# sh ip route
```

#### Display Routing Protocols

```shell
Router# sh ip protocols
```

#### Add Static Route

```bash
Router(config)# ip route ${DESTINATION_NETWORK} ${NETMASK} ${GATEWAY}
$OR
Router(config)# ip route ${DESTINATION_NETWORK} ${NETMASK} ${INTERFACE}
```

## Dynamic Routing

#### Enable RIPv2

```shell
Router(config)# router rip
Router(config-router)# version 2
```

#### Enable EIGRP

```shell
# Same AS number among all routers
Router(config)# router eigrp ${AS_NUMBER}
```

#### Bind Network to RIP/EIGRP

```shell
Router(config-router)# network 10.0.0.0

# For EIGRP, wildcard mask can be written
Router(config-router)# network 10.0.0.0 0.255.255.255
```

* Router's interfaces with IP 10.X.X.X are **allowed to send and receive** routes
* Router can send routes for the network 10.0.0.0

#### Display EIGRP routes

```shell
Router# sh ip route eigrp
```

#### Display EIGRP Neighbour Table

```shell
Router# sh ip eigrp neighbors
```

#### No Auto-summary

```shell
Router(config-router)# no auto-summary
```

#### Enable OSPF

```shell
Router(config)# router ospf ${LOCAL_NUMBER}
```

#### Bind Network to OSPF

```shell
Router(config-router)# network 10.0.0.0 0.255.255.255 area 0
```

#### OSPF Priority

```shell
Router(config)# int f0/0
Router(config-if)# ip ospf priority ${0-255}
```

#### Display OSPF Neighbour Table

```shell
Router# sh ip ospf neighbor
```

#### Change OSPF Hello / Dead Interval

```shell
Router(config-if)# ip ospf hello-interval 15

Router(config-if)# ip ospf dead-interval 30
```

#### Display OSPF Topological Database

```shell
Router# sh ip ospf database
```

## Access Lists

#### Create Named Access List

```shell
Router(config)# ip access-list $(standard/extended) ${NAME}
Router(config-std-nacl)# deny 10.0.0.0 0.255.255.255
Router(config-std-nacl)# permit any
Router(config-std-nacl)# int f0/0
Router(config-if)# ip access-group ${NAME} $(in/out)
```

#### Define Standard IP Access List

```shell
# Deny 192.168.1.3 to access list 1
Router(config)# access-list 1 deny 192.168.1.3 0.0.0.0

# Permit 10.0.0.0/8 to access list 1
Router(config)# access-list 1 permit 10.0.0.0 0.255.255.255
```

#### Define Extened IP Access List

```shell
Router(config)# access-list 100 deny tcp 0.0.0.0 255.255.255.255 192.168.1.0 0.0.0.255 eq 23

Router(config)# access-list 100 deny icmp any 192.168.1.0 0.0.0.255 echo

access-list ${100-199} $(permit/deny) ${TCP/UDP} ${SOURCE_NETWORK} ${SOURCE_MASK} $(eq ${PORT}) ${DEST_NETWORK} ${DEST_MASK} $(eq ${PORT})
```

#### Apply Access List to Interface

```shell
Router(config-if)# ip access-group 1 out
# Applied only on outbound data flowing out from router
```

#### Display Access Lists

```shell
Router# sh access-lists
```

#### Display Access Lists Applied Interface

```shell
Router# sh ip interface
```

## Wide Area Networking (WAN) Protocols

#### Changing Encapsulation Types

```shell
Router(config-if)# encapsulation ppp
```

#### Configure PPP Authentication

```shell
Router(config-if)# ppp encapsulation $(chap/pap)

Router(config-if)# ppp encapsulation chap pap
# If opposite side does not support CHAP, then use PAP
```

## Cisco Discovery Protocol (CDP)

#### Display Neighbors Information

```shell
Router# sh cdp neighbors
```

#### Display Neighbors Information in details

```shell
Router# sh cdp neighbors detail
```

## NAT, DHCP and NTP

#### Configure NAT

```shell
Router(config)# int f0/0
Router(config-if)# ip nat outside
# Connecting to outside network

Router(config)# int f0/1
Router(config-if)# ip nat inside
# Connecting to inside network
```

#### Configure NAT Translation

```shell
Router(config)# ip nat pool ${NAME} ${START_IP_RANGE} ${END_IP_RANGE} netmask ${NETMASK}

Router(config)# ip nat inside source list ${ACCESS_LIST_NUMBER} pool ${NAME} overload
# NAT router should translate source address that matches access list to public address from address pool
```

#### Static NAT Configuration

```shell
Router(config)# ip nat inside source static $(tcp/udp) ${INTERNAL_IP} ${PORT} ${PUBLIC_IP} ${PORT}
```

#### Display NAT Translation Table

```shell
Router# sh ip nat translations
```

#### DHCP Configuration

```shell
Router(config)# ip dhcp excluded-address ${START_IP} ${END_IP}
Router(config)# ip dhcp pool ${NAME}
Router(dhcp-config)# network 10.0.0.0 /8
Router(dhcp-config)# default-router 10.0.0.1
Router(dhcp-config)# dns-server ${FIRST_DNS} ${SECOND_DNS}
```

#### Get IP from DHCP

```shell
Router(config-if)# ip address dhcp
```

#### Display DHCP Report

```shell
Router# sh dhcp lease

Router# sh dhcp server
```

#### NTP Configuration

```shell
Router(config)# ntp server ${SERVER_DOMAIN}
Router(config)# clock timezone ${REGION} ${UTC+?}
```

#### Display Clock Information

```shell
Router# sh clock
```

#### Display IP Addresses Conflict

```shell
Router# sh ip dhcp conflict
```

#### Remove Conflict IP Entry

```shell
Router# clear ip dhcp conflict ${IP_ADDRESS}
```

## IP Version 6

#### Enable IPv6 Routing

```shell
Router(config)# ipv6 unicast-routing
```

#### Configure IPv6 Address

```shell
Router(config)# ipv6 address 2001:0:0:1::1/64
```

#### Display IPv6 Information

```shell
Router# sh ipv6 int f0/0
```

#### IPv6 Ping Request

```shell
Router# ping ipv6 2001:0:0:1::1
```

#### IPv6 Routing Table

```shell
Router# sh ipv6 route
```

#### Configure IPv6 Static Route

```shell
Router(config)# ipv6 route 2001:0:0:2::/64 2001:0:0:1::1
```

#### OSPFv3 Configuration

```shell
Router(config-if)# ipv6 ospf 1 area 0

# Config router ID
Router(config)# ipv6 router ospf 1
Router(config-rtr)# router-id ${IPv4_ADDRESS}
```

#### IPv6 Anycast Configuration

```shell
Router(config-if)# ipv6 address ${IPv6_ADDRESS} anycast
```

#### Generic Routing Encapsulation (GRE) Tunnel Configuration

```shell
Router(config)# int tu0
Router(config-if)# ipv6 enable
Router(config-if)# tunnel source ${IPv4_INTERFACE}
Router(config-if)# tunnel destination ${DEST_IPv4_ADDRESS}

# Static route through virtual tunnel interface 0
Router(config)# ipv6 route 2001:0:0:1::/64 tu0
```

## Secure Shell (SSH) Access

#### SSH Server Configuration

```shell
Router(config)# hostname ${HOSTNAME}
Router(config)# ip domain-name ${DOMAIN}

Router(config)# crypto key generate rsa
768

Router(config)# username ${USERNAME} password ${PASSWORD}
Router(config)# line vty 0 4
Router(config-line)# login local
```

## System Message Logging (SYSLOG)

#### Display Logging Information

```shell
Router# sh logging
```

#### Time Display for Logging Message

```shell
Router(config)# service timestamps log datetime msec
# Only affects all level 6 and lower level messages

Router(config)# service timestamps debug datetime msec
# Only affects all level 7 level messages
```

#### SYSLOG to Server

```shell
Router(config)# logging host ${IP_ADDRESS}
```

## Simple Network Management Protocol (SNMP)

#### SNMP Sample Configuration 1

```shell
Router(config)# snmp-server community ${STRING} $(ro/rw)
# ro: read-only, rw: read-write
```

#### SNMP Sample Configuration 2

```shell
Router(config)# snmp-server host ${SNMP_MANAGER} informs version 2c ${STRING} ospf
```

## Quality of Service (QoS)

#### DSCP Configuration

```shell
Router(config)# class-map ${CLASS_NAME}
Router(config-class)# match access-group ${ACCESS_LIST}

Router(config-class)# policy-map ${POLICY_NAME}
Router(config-policy)# class ${CLASS_NAME}
Router(config-policy)# set ip dscp ef

Router(config)# int f0/0
Router(config-if)# service-policy input ${POLICY_NAME}
```

#### Enable Random Dropping

```shell
Router(config-class)# policy-map ${POLICY_NAME}
Router(config-policy)# class ${CLASS_NAME}
Router(config-policy)# bandwidth ${Kbps}
Router(config-policy)# random-detect dscp-based
```

#### VoIP Configuration

```shell
Switch(config-if)# switchport access vlan 2
Switch(config-if)# switchport voice vlan 200
```

## IP Service Level Agreement (SLA)

#### Display IP SLA Statistics

```shell
Router# sh ip sla statistics
```

#### Test Profile Configuration

```shell
Router(config)# ip sla 1
Router(config)# icmp-echo ${IP_ADDRESS}
Router(config)# frequency ${SECONDS}

Router(config)# ip sla 2
Router(config)# icmp-jitter ${IP_ADDRESS}
Router(config)# frequency ${SECONDS}
```

## AAA with TACACS+ and RADIUS

#### AAA Configuration

```shell
# Enable AAA
Router(config)# aaa new-model

Router(config)# tacacs-server host ${HOST_IP}
Router(config)# tacacs-server key ${STRING}

Router(config)# aaa authentication login ${LISTNAME} group tacacs+

Router(config)# line vty 0 4
Router(config)# login authentication ${LISTNAME}
```

## Multi-Protocol Label Switching (MPLS)

#### Fast Switching Cache Table

```shell
Router# sh ip cache verbose
```

#### Cisco Express Forwarding (CEF) Table

```sh
Router# sh ip cef
```

## Hot Standby Routing Protocol (HSRP)

#### HSRP Configuration

```shell
# Hosts in network should be configured default gateway with ${VIRTUAL_IP}
Router(config-if)# standby ${STANDBY_GROUP} ip ${VIRTUAL_IP}

# Rotuer with higher HSRP priority becomes HSRP active router
Router(config-if)# standby ${STANDBY_GROUP} priority ${PRIORITY_NUMBER}

Router(config-if)# standby ${STANDBY_GROUP} preempt
```

#### Display HSRP Information

```shell
Router# sh standby
```

## Border Gateway Protocol (BGP)

#### Enable BGP

```shell
Router(config)# router bgp ${AS_NUMBER}
```

#### Neighbour Configuration

```shell
Router(config)# neighbor ${IP_ADDRESS} remote-as ${NEIGHBOR_AS}
```

#### Bind Network to BGP

```shell
Router(config-router)# network 10.0.0.0
# Announce route 10.0.0.0/8 to its BGP peer if it has this route in the routing table
```

#### Display BGP Information

```shell
Router# sh ip bgp
$OR
Router# sh ip bgp summary
```

## Password Recovery

#### Not to Load Configuration in NVRAM

```shell
rommon 1 > confreg 0x2142
# bypass startup configuration after reboot
```

#### Load Configuration in NVRAM

```shell
rommon 1 > confreg 0x2102
# load startup configuration after reboot
```

## Miscellaneous

#### User Inactivity Timeout Configuration

```shell
# Console connection
Router(config)# line console 0
# First 5 remote connection
Router(config)# lint vty 0 4

# Can be used in different connections
Router(config-line)# exec-timeout ${MINUTES} ${SECONDS}
```